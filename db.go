package main

import (
	"context"
	"fmt"

	"github.com/jackc/pgx/v5"
)

func ConnDB() error {
	const (
		host     = "grud-pgsql-db"
		port     = 5432
		user     = "postgres"
		password = "postgres"
		dbname   = "postgres"
	)

	// connString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, dbname)
	connString := fmt.Sprintf("postgres://%s:%s@%s:%d/%s", user, password, host, port, dbname)

	// urlExample := "postgres://username:password@localhost:5432/database_name"
	db, err := pgx.Connect(context.Background(), connString)
	if err != nil {
		// panic(err)
		return err
	}
	defer db.Close(context.Background())

	if err = db.Ping(context.Background()); err != nil {
		// if err != nil {
		// panic(err)
		return err
	}

	return nil
}
