package main

import (
	"fmt"
	"log/slog"
	"net/http"
	"os"
	"time"
)

func awaitDB() {
	if err := ConnDB(); err != nil {
		slog.Warn("Could not reach database", "err", err)
		// os.Exit(1)
		time.Sleep(1 * time.Second)
		awaitDB()
	}
}

func main() {
	logger := slog.New(slog.NewTextHandler(os.Stdout, nil))
	slog.SetDefault(logger)

	port := os.Getenv("PORT")
	if port == "" {
		port = "8080"
	}

	awaitDB()
	slog.Info("connected to database")

	http.HandleFunc("/", greetEndpoint)
	// Start the server
	slog.Info("starting server at :" + port)
	if err := http.ListenAndServe(":"+port, nil); err != nil {
		slog.Error("Could not start server", "err", err)
		os.Exit(1)
	}
}

func greetEndpoint(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintf(w, "Hello, %s!", r.URL.Path[1:])
}
