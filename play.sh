#!/bin/bash
set -eu

podman kube play \
  --replace \
  --annotation="commit=$(jj log --template='commit_id' --no-graph -r @)" \
  kube.yaml
