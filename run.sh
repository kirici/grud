#!/bin/bash
set -eu

# Global consts
HOST_PORT=8080
CTR_PORT=8080

# Global vars
SCRIPT_PATH="$(dirname "$(realpath "$0")")"
PROJECT_NAME="${SCRIPT_PATH##*/}"

podman run \
  -td \
  --replace \
  -e PORT="${CTR_PORT}" \
  -p "${HOST_PORT}":"${CTR_PORT}" \
  --name "${PROJECT_NAME}" \
  "${PROJECT_NAME}":latest
